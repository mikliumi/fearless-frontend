


function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
    <div class='col'>
        <div class="card shadow p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
            <footer class="card-footer"> ${startDate} - ${endDate} </footer>
        </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

      if (!response.ok) {
        const container = document.querySelector('.container');
        container.innerHTML += "<div class='alert alert-primary' role='alert'>What did you do you monster!????!?!?!?!</div>";
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;

            const pictureUrl = details.conference.location.picture_url;

            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const startDate = new Date(starts).toLocaleDateString();
            const endDate = new Date(ends).toLocaleDateString();

            const locationName = details.conference.location.name;

            const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.log(e);
    }

  });
