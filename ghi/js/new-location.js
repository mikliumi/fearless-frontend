window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/"

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        console.log(data);
        const selectTag = document.getElementById('state');
        for (let state of data.states){
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }
    }


    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        // this functions on the name tag to generate the key in the data object that correpsonds to this html element
        // because i didnt have a name on 'state', it wasnt grabbing it
        // state is mandatory in this case
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        console.log(response);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });
});
